# Trainee Program - Week 3

## Installation

To run the project you should follow the next stepts

- Download the project
- Install globally the `json-server` package. You can find it [here](https://www.npmjs.com/package/json-server)
- You must run the following command to install `json-server` globally `npm i -g json-server`
- Then you already installed the `json-server` package, you must run inside the project folder `json-server db.json`

This will start a local server on your `localhost` in the port `3000`

Now your ready to open any of the links below to see the final version of the project

## Deployment

You can see the last deployed version of the project by going to any of these links

[Vercel](https://week-3.b-mendoza.vercel.app/)  
[Netlify](https://b-mendoza-week-3.netlify.app/)
