import HTTPSingleton from '../classes/HTTPSingleton.js';

const TAGS_REQUESTS = new HTTPSingleton();
const BASE_ENDPOINT = 'tags';

export const getTags = async () => {
  const endpoint = BASE_ENDPOINT;

  return await TAGS_REQUESTS.get(endpoint);
};

export const getTagsByIdList = async idList => {
  const endpoint = `${BASE_ENDPOINT}?id_like=${encodeURI(`[${idList}]`)}`;

  return await TAGS_REQUESTS.get(endpoint);
};
