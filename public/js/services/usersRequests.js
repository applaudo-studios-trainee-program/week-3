import HTTPSingleton from '../classes/HTTPSingleton.js';

const USERS_REQUESTS = new HTTPSingleton();
const BASE_ENDPOINT = 'users';

export const getUsers = async () => {
  const endpoint = BASE_ENDPOINT;

  return await USERS_REQUESTS.get(endpoint);
};

export const getUsersByIdList = async usersList => {
  const endpoint = `${BASE_ENDPOINT}?id_like=${encodeURI(`[${usersList}]`)}`;

  return await USERS_REQUESTS.get(endpoint);
};
