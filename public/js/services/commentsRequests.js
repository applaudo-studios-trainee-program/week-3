import HTTPSingleton from '../classes/HTTPSingleton.js';

const COMMENTS_REQUESTS = new HTTPSingleton();
const BASE_ENDPOINT = 'comments';

export const getCommentsByPostId = async postId => {
  const endpoint = `${BASE_ENDPOINT}?postId_like=${postId}`;

  return await COMMENTS_REQUESTS.get(endpoint);
};

export const postComment = async postData => {
  const endpoint = BASE_ENDPOINT;

  await COMMENTS_REQUESTS.post(endpoint, { ...postData });
};
