import HTTPSingleton from '../classes/HTTPSingleton.js';
import { getPostById } from './postsRequests.js';

const LIKES_REQUESTS = new HTTPSingleton();

export const putLikesByPostId = async postId => {
  const postData = await getPostById(postId);

  const endpoint = `posts/${postId}`;

  LIKES_REQUESTS.put(endpoint, { ...postData, likes: postData.likes + 1 });
};
