import HTTPSingleton from '../classes/HTTPSingleton.js';

const POSTS_REQUESTS = new HTTPSingleton();
const BASE_ENDPOINT = 'posts';

export const getPosts = async () => {
  const endpoint = BASE_ENDPOINT;

  return await POSTS_REQUESTS.get(endpoint);
};

export const getPostsByOrder = async order => {
  const endpoint = `${BASE_ENDPOINT}?_sort=createDate&_order=${order}`;

  return await POSTS_REQUESTS.get(endpoint);
};

export const getPostsByTag = async tagId => {
  const endpoint = `${BASE_ENDPOINT}?tags_like=${tagId}`;

  return await POSTS_REQUESTS.get(endpoint);
};

export const getLastThreePosts = async () => {
  const postList = await getPosts();

  const endpoint = `${BASE_ENDPOINT}?_start=${postList.length - 3}&_limit=3`;

  return await POSTS_REQUESTS.get(endpoint);
};

export const getPostsByTitle = async titleSearch => {
  const endpoint = `${BASE_ENDPOINT}?title_like=${titleSearch}`;

  return await POSTS_REQUESTS.get(endpoint);
};

export const getPostById = async id => {
  const endpoint = `${BASE_ENDPOINT}/${id}`;

  return await POSTS_REQUESTS.get(endpoint);
};

export const getRemainingPosts = async () => {
  const postList = await getPosts();

  const endpoint = `${BASE_ENDPOINT}?_start=0&_limit=${postList.length - 3}`;

  return await POSTS_REQUESTS.get(endpoint);
};

export const deletePostById = postId => {
  const endpoint = `${BASE_ENDPOINT}/${postId}`;

  POSTS_REQUESTS.delete(endpoint);
};

export const putPostById = postData => {
  const { id } = postData;

  const endpoint = `${BASE_ENDPOINT}/${id}`;

  POSTS_REQUESTS.put(endpoint, { ...postData });
};

export const postPost = async postData => {
  const endpoint = BASE_ENDPOINT;

  await POSTS_REQUESTS.post(endpoint, { ...postData });
};
