import HTTPSingleton from '../classes/HTTPSingleton.js';

const AUTHORS_REQUESTS = new HTTPSingleton();
const BASE_ENDPOINT = 'authors';

export const getAuthorById = async authorId => {
  const endpoint = `${BASE_ENDPOINT}?id_like=${authorId}`;

  return await AUTHORS_REQUESTS.get(endpoint);
};
