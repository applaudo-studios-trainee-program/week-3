export const errorHandler = () => {
  const error = document.querySelector('.error');
  const isErrorInDOM = document.body.contains(error);

  if (isErrorInDOM) error.remove();
  else {
    const error = document.createElement('div');
    const errorImage = document.createElement('img');
    const errorTitle = document.createElement('h1');
    const errorContent = document.createElement('h2');
    const errorLink = document.createElement('a');

    errorLink.classList.add('error__link');
    errorLink.setAttribute('href', '/');
    errorLink.append('Go back Home');

    errorContent.classList.add('error__content');
    errorContent.append(errorLink);

    errorTitle.classList.add('error__title');
    errorTitle.append('Looks like occurred an error 😞');

    errorImage.classList.add('error__image');
    errorImage.setAttribute(
      'src',
      'https://media4.giphy.com/media/BEob5qwFkSJ7G/200.webp?cid=ecf05e4743cxb2sqf65om1m6yqjk4hadnjqxvmxlkr59y1ns&rid=200.webp'
    );
    errorImage.setAttribute('alt', 'Error GIF');
    errorImage.setAttribute('title', 'Error');

    error.classList.add('error');
    error.append(errorImage);
    error.append(errorTitle);
    error.append(errorContent);

    document.body.append(error);
  }
};
