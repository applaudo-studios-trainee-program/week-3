import { deletePostById } from '../services/postsRequests.js';

export const actionsBuilder = ({ referenceNode, postId }) => {
  const editActionLink = document.createElement('a');
  const editAction = document.createElement('button');
  const deleteAction = document.createElement('button');

  editAction.classList.add('main-actions__button', 'button--edit');
  editAction.append('Edit');

  deleteAction.classList.add('main-actions__button', 'button--delete');
  deleteAction.append('Delete');
  deleteAction.addEventListener('click', () => {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: 'Deleted!',
          text: 'The post has been deleted.',
          icon: 'success',
          timer: 1000,
          showConfirmButton: false,
        });

        setTimeout(() => {
          deletePostById(postId);

          window.location.href = '../../';
        }, 1200);
      }
    });
  });

  editActionLink.classList.add('main-actions__edit-link');
  editActionLink.setAttribute('href', `./post-manager.html?postId=${postId}`);
  editActionLink.append(editAction);

  referenceNode.append(editActionLink);
  referenceNode.append(deleteAction);
};
