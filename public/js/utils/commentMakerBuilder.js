// Builds the comment section

import { getUsers } from '../services/usersRequests.js';

export const commentMakerBuilder = async ({ referenceNode }) => {
  const userList = await getUsers();

  const formCommentMaker = document.createElement('form');
  const formCommentMakerContainer = document.createElement('div');
  const formCommentMakerLabel = document.createElement('label');
  const formCommentMakerSelect = document.createElement('select');
  const formCommentMakerInput = document.createElement('input');
  const formCommentMakerButton = document.createElement('button');

  formCommentMakerButton.classList.add('form-comment-maker__button');
  formCommentMakerButton.append('Post comment');

  formCommentMakerInput.classList.add('form-comment-maker__input');
  formCommentMakerInput.setAttribute('type', 'text');

  formCommentMakerLabel.classList.add('form-comment-maker__label');
  formCommentMakerLabel.setAttribute('for', 'user-comment');
  formCommentMakerLabel.append('@');

  formCommentMakerSelect.classList.add('form-comment-maker__select');
  formCommentMakerSelect.setAttribute('id', 'user-comment');

  userList.map((userData) => {
    const userOption = document.createElement('option');

    const { id, name, lastName } = userData;
    const userName = `${name} ${lastName}`;

    userOption.setAttribute('value', id);
    userOption.append(userName);

    formCommentMakerSelect.append(userOption);
  });

  formCommentMakerContainer.classList.add('form-comment-maker__container');
  formCommentMakerContainer.append(formCommentMakerLabel);
  formCommentMakerContainer.append(formCommentMakerSelect);

  formCommentMaker.classList.add('form-comment-maker');
  formCommentMaker.append(formCommentMakerContainer);
  formCommentMaker.append(formCommentMakerInput);
  formCommentMaker.append(formCommentMakerButton);

  referenceNode.append(formCommentMaker);

  return formCommentMaker;
};
