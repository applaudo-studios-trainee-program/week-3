import {
  postComment,
  getCommentsByPostId,
} from '../services/commentsRequests.js';
import { putLikesByPostId } from '../services/likesRequests.js';
import { getUsers } from '../services/usersRequests.js';
import { commentMakerBuilder } from './commentMakerBuilder.js';
import { commentSetter } from './commentSetter.js';
import { getId } from './getId.js';
import { tagSetter } from './tagSetter.js';

export const mainPostBuilder = async ({
  referenceNode,
  id: postId,
  title,
  subTitle,
  image: imageUrl,
  body: content,
  createDate: creationDate,
  likes,
  author,
  tagList,
  commentList,
}) => {
  const usersList = await getUsers();

  const mainPostInfoData = [
    {
      elements: [
        {
          tagType: 'span',
          classList: ['main-post__info-icon', 'fas', 'fa-heart', 'fa-lg'],
        },
        {
          tagType: 'p',
          classList: ['main-post__info-text'],
          content: likes,
        },
      ],
    },
    {
      elements: [
        {
          tagType: 'span',
          classList: ['main-post__info-icon', 'fas', 'fa-clock', 'fa-lg'],
        },
        {
          tagType: 'p',
          classList: ['main-post__info-text'],
          content: creationDate,
        },
      ],
    },
    {
      elements: [
        {
          tagType: 'span',
          classList: ['main-post__info-icon', 'fas', 'fa-at', 'fa-lg'],
        },
        {
          tagType: 'p',
          classList: ['main-post__info-text'],
          content: author,
        },
      ],
    },
  ];

  const mainPostTitle = document.createElement('h1');
  const mainPostSubTitle = document.createElement('p');
  const mainPostImage = document.createElement('img');
  const mainPostBody = document.createElement('p');
  const mainPostInfo = document.createElement('section');
  const mainPostTagList = document.createElement('section');
  const mainPostCommentList = document.createElement('section');
  const mainPostCommentListTitle = document.createElement('h2');
  const mainPostCommentMaker = document.createElement('section');
  const mainPostCommentMakerTitle = document.createElement('h2');

  mainPostTitle.classList.add('main__title', 'main-post__title');
  mainPostTitle.append(title);

  mainPostSubTitle.classList.add('main-post__sub-title');
  mainPostSubTitle.append(subTitle);

  mainPostImage.classList.add('main-post__image');
  mainPostImage.setAttribute('alt', `${title} Post`);
  mainPostImage.setAttribute('loading', 'lazy');
  mainPostImage.setAttribute('src', imageUrl);
  mainPostImage.setAttribute('title', title);

  mainPostBody.classList.add('main-post__body');
  mainPostBody.append(content);

  mainPostInfo.classList.add('main-post__info');

  // Set the elements that contains the info of the post
  mainPostInfoData.map(({ elements }, index) => {
    const mainPostInfoContainer = document.createElement('div');

    elements.map((elementData, index) => {
      if (index === 0) {
        const { tagType, classList } = elementData;

        const mainPostInfoIcon = document.createElement(tagType);

        classList.map(className => mainPostInfoIcon.classList.add(className));

        mainPostInfoContainer.append(mainPostInfoIcon);
      } else {
        const { tagType, classList, content } = elementData;

        const mainPostInfoText = document.createElement(tagType);

        classList.map(className => mainPostInfoText.classList.add(className));

        mainPostInfoText.append(content);
        mainPostInfoContainer.append(mainPostInfoText);
      }
    });

    if (index === 0) {
      mainPostInfoContainer.addEventListener('click', async function () {
        await putLikesByPostId(postId);

        document.querySelector('.main-post__info-text').innerHTML++;
      });
    }

    mainPostInfoContainer.classList.add('main-post__info-container');
    mainPostInfo.append(mainPostInfoContainer);
  });

  mainPostTagList.classList.add('main-post__tag-list', 'tag-list');

  // Set the tags
  tagList.map(tagData =>
    tagSetter({ referenceNode: mainPostTagList, ...tagData })
  );

  mainPostCommentListTitle.classList.add('main-post__comment-list-title');
  mainPostCommentListTitle.append('Comments');

  mainPostCommentList.classList.add('main-post__comment-list');
  mainPostCommentList.append(mainPostCommentListTitle);

  // Handles the update when a comment is made
  commentList.map(commentData => {
    const { name, lastName } = usersList.find(
      user => commentData.user === user.id
    );

    const userName = `${name} ${lastName}`;

    commentSetter({
      referenceNode: mainPostCommentList,
      ...commentData,
      userName,
    });
  });

  mainPostCommentMakerTitle.classList.add('main-post__comment-maker-title');
  mainPostCommentMakerTitle.append('Make a comment');

  mainPostCommentMaker.classList.add('main-post__comment-maker');
  mainPostCommentMaker.append(mainPostCommentMakerTitle);

  // Makes the comment section
  const mainCommentMaker = await commentMakerBuilder({
    referenceNode: mainPostCommentMaker,
  });

  // Updates the UI when a comment is made
  mainCommentMaker.addEventListener('submit', async event => {
    event.preventDefault();

    const user = parseFloat(
      document.querySelector('.form-comment-maker__select').value
    );

    const comment = document
      .querySelector('.form-comment-maker__input')
      .value.trim();

    if (comment !== '') {
      document.querySelector('.form-comment-maker__input').value = '';
      const id = getId();

      await postComment({ id, comment, postId, user });

      mainPostCommentList.innerHTML = '';
      mainPostCommentList.append(mainPostCommentListTitle);

      const commentList = await getCommentsByPostId(postId);

      commentList.map(commentData => {
        const { name, lastName } = usersList.find(
          user => commentData.user === user.id
        );

        const userName = `${name} ${lastName}`;

        commentSetter({
          referenceNode: mainPostCommentList,
          ...commentData,
          userName,
        });
      });
    }
  });

  referenceNode.append(mainPostTitle);
  referenceNode.append(mainPostSubTitle);
  referenceNode.append(mainPostImage);
  referenceNode.append(mainPostBody);
  referenceNode.append(mainPostInfo);
  referenceNode.append(mainPostTagList);
  referenceNode.append(mainPostCommentList);
  referenceNode.append(mainPostCommentMaker);
};
