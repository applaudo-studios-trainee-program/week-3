export const postSetter = ({
  referenceNode,
  image,
  title,
  likes,
  createDate,
}) => {
  const postDataContent = [
    {
      elements: [
        {
          tagType: 'i',
          classList: ['post__data-icon', 'fas', 'fa-heart'],
        },
        {
          tagType: 'p',
          classList: ['post__data-text'],
          content: likes,
        },
      ],
    },
    {
      elements: [
        {
          tagType: 'i',
          classList: ['post__data-icon', 'fas', 'fa-clock'],
        },
        {
          tagType: 'p',
          classList: ['post__data-text'],
          content: createDate,
        },
      ],
    },
  ];

  const post = document.createElement('div');
  const postImage = document.createElement('img');
  const postContent = document.createElement('section');
  const postTitle = document.createElement('p');
  const postData = document.createElement('section');

  postImage.classList.add('post__image');
  postImage.setAttribute('alt', `${title} Post`);
  postImage.setAttribute('loading', 'lazy');
  postImage.setAttribute('src', image);
  postImage.setAttribute('title', title);

  postTitle.classList.add('post__title');
  postTitle.append(title);

  postData.classList.add('post__data');

  postDataContent.map(({ elements }) => {
    const postDataContainer = document.createElement('div');

    elements.map((data, index) => {
      if (index === 0) {
        const { tagType, classList } = data;

        const postDataIcon = document.createElement(tagType);

        classList.map((className) => postDataIcon.classList.add(className));

        postDataContainer.append(postDataIcon);
      } else {
        const { tagType, classList, content } = data;

        const postDataText = document.createElement(tagType);

        classList.map((className) => postDataText.classList.add(className));

        postDataText.append(content);
        postDataContainer.append(postDataText);
      }
    });

    postDataContainer.classList.add('post__data-container');
    postData.append(postDataContainer);
  });

  postContent.classList.add('post__content');
  postContent.append(postTitle);
  postContent.append(postData);

  post.classList.add('post');
  post.append(postImage);
  post.append(postContent);

  referenceNode.append(post);
};
