export const tagSetter = ({ referenceNode, name, id, slug }) => {
  const tag = document.createElement('span');
  const tagInput = document.createElement('input');
  const tagContent = document.createElement('label');
  const tagIcon = document.createElement('i');

  tagInput.classList.add('tag__input');
  tagInput.setAttribute('id', id);
  tagInput.setAttribute('name', 'tag');
  tagInput.setAttribute('type', 'radio');
  tagInput.setAttribute('value', slug);

  tagIcon.classList.add('tag__icon', 'fas', 'fa-tag');

  tagContent.classList.add('tag__content');
  tagContent.setAttribute('for', id);
  tagContent.append(tagIcon);
  tagContent.append(name);

  tag.classList.add('tag');
  tag.append(tagInput);
  tag.append(tagContent);

  referenceNode.append(tag);
};
