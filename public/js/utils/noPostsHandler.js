export const noPostsHandler = ({ referenceNode }) => {
  const noPosts = document.createElement('p');

  noPosts.classList.add('empty-post-list');
  noPosts.append("We couldn't find any post 😞");

  referenceNode.append(noPosts);
};
