// Set comments

export const commentSetter = ({
  referenceNode,
  userName,
  comment: content,
}) => {
  const comment = document.createElement('div');
  const commentInfo = document.createElement('p');
  const commentBody = document.createElement('p');
  const commentIcon = document.createElement('span');

  commentIcon.classList.add('comment__icon', 'fas', 'fa-at');

  commentInfo.classList.add('comment__info');
  commentInfo.append(commentIcon);
  commentInfo.append(` ${userName}`);

  commentBody.classList.add('comment__body');
  commentBody.append(content);

  comment.classList.add('comment');
  comment.append(commentInfo);
  comment.append(commentBody);

  referenceNode.append(comment);
};
