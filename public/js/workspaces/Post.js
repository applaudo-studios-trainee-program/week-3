import { getAuthorById } from '../services/authorsRequests.js';
import { getCommentsByPostId } from '../services/commentsRequests.js';
import { getPostById } from '../services/postsRequests.js';
import { getTagsByIdList } from '../services/tagsRequests.js';
import { actionsBuilder } from '../utils/actionsBuilder.js';
import { errorHandler } from '../utils/errorHandler.js';
import { mainPostBuilder } from '../utils/mainPostBuilder.js';

const Post = async () => {
  try {
    const mainPost = document.querySelector('.main-post');
    const mainActions = document.querySelector('.main-actions');

    const urlQueryString = window.location.search;

    const postId = urlQueryString.substr(8);
    const postData = await getPostById(postId);

    const { author: authorId, tags: tagIdList } = postData;

    const [{ name: fistName, lastName }] = await getAuthorById(authorId);
    const author = `${fistName} ${lastName}`;

    const tagList = await getTagsByIdList(tagIdList);

    const commentList = await getCommentsByPostId(postId);

    await mainPostBuilder({
      referenceNode: mainPost,
      ...postData,
      author,
      tagList,
      commentList,
    });

    actionsBuilder({ referenceNode: mainActions, postId });

    if (commentList.length === 0) {
      const commentListContainer = document.querySelector(
        '.main-post__comment-list'
      );

      const noComments = document.createElement('p');

      noComments.append('There are no comments yet 😮');
      commentListContainer.append(noComments);
    }
  } catch {
    errorHandler();
  }
};

export default Post;
