import { errorHandler } from '../utils/errorHandler.js';
import { getPostsByTitle } from '../services/postsRequests.js';
import { noPostsHandler } from '../utils/noPostsHandler.js';
import { postSetter } from '../utils/postSetter.js';

const SearchPost = async () => {
  const form = document.querySelector('.post-search');
  const postListContainer = document.querySelector('.section__post-list');

  form.addEventListener('submit', async event => {
    event.preventDefault();

    const inputValue = document
      .querySelector('.post-search__input')
      .value.trim();

    if (inputValue !== '') {
      try {
        postListContainer.innerHTML = '';

        const postList = await getPostsByTitle(inputValue);

        if (postList.length > 0) {
          postList.map(postData => {
            const postLink = document.createElement('a');

            postLink.classList.add('post__link');
            postLink.setAttribute('href', `post.html?postId=${postData.id}`);

            postSetter({ referenceNode: postLink, ...postData });

            postListContainer.append(postLink);
          });
        } else noPostsHandler({ referenceNode: postListContainer });
      } catch {
        errorHandler();
      }
    }
  });
};

export default SearchPost;
