import PostFactory from '../classes/PostFactory.js';
import {
  getPostById,
  postPost,
  putPostById,
} from '../services/postsRequests.js';
import { getTagsByIdList } from '../services/tagsRequests.js';
import { factoryTypes } from '../types/factoryTypes.js';
import { errorHandler } from '../utils/errorHandler.js';
import { getId } from '../utils/getId.js';

const PostManager = async () => {
  const urlQueryString = window.location.search;
  const postId = urlQueryString.substr(8);

  const form = document.querySelector('.post-manager');
  const imageUrlElement = document.querySelector('#image-url');
  const titleElement = document.querySelector('#title');
  const subTitleElement = document.querySelector('#sub-title');
  const authorElement = document.querySelector('#author');
  const creationDateElement = document.querySelector('#creation-date');
  const contentElement = document.querySelector('#content');
  const tagListElement = [...document.querySelectorAll('[name="tag"]')];
  const submitButton = document.querySelector('.post-manager__button');

  if (postId) {
    try {
      const postData = await getPostById(postId);

      const {
        image: imageUrl,
        title,
        subTitle,
        body: content,
        createDate: creationDate,
        author: authorId,
        tags: tagIdList,
      } = postData;

      const tagList = await getTagsByIdList(tagIdList);

      imageUrlElement.value = imageUrl;
      titleElement.value = title;
      subTitleElement.value = subTitle;
      authorElement.selectedIndex = authorId - 1;
      creationDateElement.value = creationDate;
      contentElement.value = content;

      tagList.map(tagData =>
        tagListElement.map(tagElement => {
          if (tagData.id === parseFloat(tagElement.id))
            tagElement.checked = true;
        })
      );

      submitButton.append('Update');
    } catch {
      errorHandler();
    }
  } else {
    creationDateElement.value =
      `${new Date().getFullYear()}/` +
      `${new Date().getMonth()}/` +
      `${new Date().getDate()}`;

    submitButton.append('Create');
  }

  form.addEventListener('submit', async event => {
    event.preventDefault();

    const image = imageUrlElement.value.trim();
    const title = titleElement.value.trim();
    const subTitle = subTitleElement.value.trim();
    const body = contentElement.value.trim();
    const createDate = creationDateElement.value;
    const author = authorElement.selectedIndex + 1;
    const tags = tagListElement
      .filter(elementTag => elementTag.checked === true)
      .map(elementTag => parseFloat(elementTag.id));

    if (image !== '' && title !== '' && subTitle !== '' && body !== '') {
      try {
        if (postId) {
          const postData = await getPostById(postId);

          const post = new PostFactory(factoryTypes.POST, {
            ...postData,
            author,
            image,
            title,
            subTitle,
            body,
            createDate,
            tags,
          });

          putPostById({ ...post });

          Swal.fire({
            icon: 'success',
            title: 'Your post has been updated',
            showConfirmButton: false,
            timer: 1000,
          });

          setTimeout(() => {
            window.location.href = `../../views/post.html?postId=${postId}`;
          }, 1200);
        } else {
          const id = getId();

          const post = new PostFactory(factoryTypes.POST, {
            id,
            image,
            title,
            subTitle,
            body,
            createDate,
            author,
            tags,
          });

          await postPost({ ...post });

          Swal.fire({
            icon: 'success',
            title: 'Your post has been created',
            showConfirmButton: false,
            timer: 1000,
          });

          setTimeout(() => {
            window.location.href = `../../views/post.html?postId=${id}`;
          }, 1200);
        }
      } catch {
        errorHandler();
      }
    }
  });
};

export default PostManager;
