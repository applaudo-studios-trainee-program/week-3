import { errorHandler } from '../utils/errorHandler.js';
import { getTags } from '../services/tagsRequests.js';
import { noPostsHandler } from '../utils/noPostsHandler.js';
import { postSetter } from '../utils/postSetter.js';
import { tagSetter } from '../utils/tagSetter.js';
import {
  getLastThreePosts,
  getPostsByOrder,
  getPostsByTag,
  getRemainingPosts,
} from '../services/postsRequests.js';

const Home = async () => {
  try {
    // ---------------------------------------------------------------------------
    // Home - A

    const tagList = await getTags();
    const tagListContainer = document.querySelector('.section-a__tag-list');

    tagList.map(tagData =>
      tagSetter({ referenceNode: tagListContainer, ...tagData })
    );

    const tagListElements = [...document.querySelectorAll('.tag__input')];

    tagListElements.map(tagElement =>
      tagElement.addEventListener('change', async function () {
        try {
          const postList = await getPostsByTag(this.id);
          const postListContainer = document.querySelector(
            '.section-a__post-list'
          );

          postListContainer.innerHTML = '';

          if (postList.length > 0) {
            postList.map(postData => {
              const postLink = document.createElement('a');

              postLink.classList.add('post__link');
              postLink.setAttribute(
                'href',
                `views/post.html?postId=${postData.id}`
              );

              postSetter({ referenceNode: postLink, ...postData });

              postListContainer.append(postLink);
            });
          } else noPostsHandler({ referenceNode: postListContainer });
        } catch {
          errorHandler();
        }
      })
    );

    // ---------------------------------------------------------------------------
    // Home - B

    const lastThreePostList = await getLastThreePosts();
    const lastThreePostListContainer = document.querySelector(
      '.section-b__post-list'
    );

    lastThreePostList.map(postData => {
      const postLink = document.createElement('a');

      postLink.classList.add('post__link');
      postLink.setAttribute('href', `views/post.html?postId=${postData.id}`);

      postSetter({ referenceNode: postLink, ...postData });

      lastThreePostListContainer.append(postLink);
    });

    // ---------------------------------------------------------------------------
    // Home - C

    const remainingPostList = await getRemainingPosts();
    const remainingPostListContainer = document.querySelector(
      '.section-c__post-list'
    );

    remainingPostList.map(postData => {
      const postLink = document.createElement('a');

      postLink.classList.add('post__link');
      postLink.setAttribute('href', `views/post.html?postId=${postData.id}`);

      postSetter({ referenceNode: postLink, ...postData });

      remainingPostListContainer.append(postLink);
    });

    // ---------------------------------------------------------------------------
    // Home - D

    const descPostList = await getPostsByOrder('desc');
    const descPostListContainer = document.querySelector(
      '.section-d__post-list'
    );

    descPostList.map(postData => {
      const postLink = document.createElement('a');

      postLink.classList.add('post__link');
      postLink.setAttribute('href', `views/post.html?postId=${postData.id}`);

      postSetter({ referenceNode: postLink, ...postData });

      descPostListContainer.append(postLink);
    });
  } catch {
    errorHandler();
  }
};

export default Home;
