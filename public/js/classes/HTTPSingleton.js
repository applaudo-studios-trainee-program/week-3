class HTTPSingleton {
  static instance;
  static baseUrl = 'http://localhost:3000/';
  static headers = { 'Content-Type': 'application/json' };

  constructor() {
    if (!HTTPSingleton.instance) HTTPSingleton.instance = this;

    return HTTPSingleton.instance;
  }

  async get(endpoint) {
    const response = await fetch(`${HTTPSingleton.baseUrl}${endpoint}`);

    if (response.status === 200) return await response.json();
    else throw new Error();
  }

  post(endpoint, data) {
    return fetch(`${HTTPSingleton.baseUrl}${endpoint}`, {
      method: 'POST',
      headers: HTTPSingleton.headers,
      body: JSON.stringify(data),
    });
  }

  put(endpoint, data) {
    return fetch(`${HTTPSingleton.baseUrl}${endpoint}`, {
      method: 'PUT',
      headers: HTTPSingleton.headers,
      body: JSON.stringify(data),
    });
  }

  delete(endpoint) {
    return fetch(`${HTTPSingleton.baseUrl}${endpoint}`, { method: 'DELETE' });
  }
}

export default HTTPSingleton;
