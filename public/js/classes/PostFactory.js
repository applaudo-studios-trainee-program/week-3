import { factoryTypes } from '../types/factoryTypes.js';

class PostFactory {
  constructor(type, props) {
    switch (type) {
      case factoryTypes.POST:
        return new Post({ ...props });
    }
  }
}

class Post {
  constructor({
    author,
    body,
    createDate,
    id,
    image,
    subTitle,
    tags,
    title,
    likes = 0,
  }) {
    this.author = author;
    this.body = body;
    this.createDate = createDate;
    this.id = id;
    this.image = image;
    this.likes = likes;
    this.subTitle = subTitle;
    this.tags = tags;
    this.title = title;
  }
}

export default PostFactory;
