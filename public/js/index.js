import Home from './workspaces/Home.js';
import SearchPost from './workspaces/SearchPost.js';
import PostManager from './workspaces/PostManager.js';
import Post from './workspaces/Post.js';

{
  document.addEventListener('DOMContentLoaded', () => {
    const workspace = document.body.id;

    switch (workspace) {
      case 'home':
        Home();
        break;
      case 'search-post':
        SearchPost();
        break;
      case 'post-manager':
        PostManager();
        break;
      case 'post':
        Post();
        break;
    }
  });
}
